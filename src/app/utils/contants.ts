export enum Direction{
  UNDEFINED = 0,
  LEFT = 1,
  UP = 2,
  RIGHT = 4,
  DOWN = 8
}
export function oppositeDirection(dir: Direction){
  switch(dir){
    case Direction.LEFT: return Direction.RIGHT;
    case Direction.RIGHT: return Direction.LEFT;
    case Direction.UP: return Direction.DOWN;
    case Direction.DOWN: return Direction.UP;
    case Direction.UNDEFINED: return Direction.UNDEFINED;
  }
}
export function getDirectionAngle(dir: Direction){
  let angle = 0;
  switch(dir){
    case Direction.RIGHT:
      angle += Math.PI/2;
    case Direction.DOWN:
      angle += Math.PI/2;
    case Direction.LEFT:
      angle += Math.PI/2;
    case Direction.UP:
      return angle;
  }
}
