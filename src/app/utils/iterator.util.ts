export class IteratorUtil{
  public static mapper<T, K>(iterable: Iterable<T>) {
    return function *(fn: (t: T)=>K) {
      for(const item of iterable){
        yield fn(item);
      }
    }
  }
  public static map<T, K>(fn: (t: T)=>K){
    return function *(iterable: Iterable<T>){
       for(const item of iterable){
        yield fn(item);
      }
    }
  }
  public static continiousMap<T, K>(fn: (a: T, b: T)=>K){
    return function *(iterable: Iterable<T>){
      let last = null;
       for(const item of iterable){
        if(last!=null){
          yield fn(last, item);
        }
        last = item;
      }
    }
  }
}
