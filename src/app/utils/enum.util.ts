export class EnumUtil {
  public static bitCheckAnd(testable: number, value: number){
    return (testable & value) === value;
  }
  public static bitCheckOr(testable: number, value: number){
    return (testable & value) !== 0;
  }

}
