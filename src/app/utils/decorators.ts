export function NumericCache(target, propertyKey, descriptor) {
  descriptor.oldFn = descriptor.value;
  descriptor.table = new Map<string, any>()
  descriptor.value = (...args: number[]) => {
    return descriptor.table.get(args.join(':')) || (x => (descriptor.table.set(args.join(':'), x),x))(descriptor.oldFn(...args))
  }

}
