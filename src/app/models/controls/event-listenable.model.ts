export interface EventListenable{
  addEventListener(name: string, listener: (event: Event)=>any): void;
  removeEventListener(name: string, listener: (event: Event)=>any): void;
}
