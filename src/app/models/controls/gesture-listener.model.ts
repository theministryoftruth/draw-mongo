import { EventEmitter } from '@angular/core';
import { EventQuery } from './QueryObject';
import { GestureEvent } from './gesture-event.model';
export class GestureListener{
  public readonly emitter: EventEmitter<GestureEvent<any>> = new EventEmitter();
  public eventHandlers: ({handler: EventListener, eventName: string})[];
  private currentQuery: EventQuery;
  private eventQuery: EventQuery;
  private wasAborted = false;
  public constructor(eventQuery: EventQuery){
    this.eventQuery = GestureListener.wrapQuery(eventQuery);
    this.flush()
  }
  private static wrapQuery(query: EventQuery){
    const last = query.last();
    last.tail = new EventQuery([]);
    return query;
  }
  public check(eventName: string, value: Event){
    if(this.currentQuery.isCurrentEvent(eventName)){
      return this.updateFiredEvents(eventName, value);
    }
    else{
      return this.flush();
    }
  }
  public flush(){
    return (this.currentQuery = this.eventQuery);
  }
  private updateFiredEvents(eventName: string, event: Event) {
    this.emit(eventName, event);
    if(this.wasAborted){
      this.wasAborted = false;
      return this.currentQuery;
    }
    const next = this.moveNext(eventName);
    if (this.allEventsFired()) {
      return this.flush();
    }
    else return next;
  }
  private emit(eventName: string, event: Event) {
    const isLastQuery = this.allEventsFired();
    const isFirstQuery = this.currentQuery == this.eventQuery;
    this.emitter.emit(this.factoryControlData(eventName, event, isFirstQuery ? GestureEventIndex.FIRST : isLastQuery ? GestureEventIndex.LAST : GestureEventIndex.MIDDLE));
  }

  private moveNext(byEvent: string) {
    return this.currentQuery = this.currentQuery.next(byEvent);
  }

  private factoryControlData(type: string, childEvent: Event, index: GestureEventIndex): GestureEvent<any>{
    return new GestureEvent(childEvent, type, index, ()=>{
      this.wasAborted = true;
      this.flush();
    })
  }
  private allEventsFired(){
    return this.currentQuery.tail == null;
  }
}
export enum GestureEventIndex {
  FIRST,
  MIDDLE,
  LAST
}
