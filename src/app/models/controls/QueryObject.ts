
export enum EventModificator {
  None,
  OneOrMore,
  ZeroOrMore
}
export type EventQueryValue = {
  name: string,
  modificator: EventModificator
}
const split = (str: string, divider: string|RegExp) => str.split(divider).map(x=>x.trim());
const isRepeatableModificator = (m: EventModificator) => m == EventModificator.ZeroOrMore || m == EventModificator.OneOrMore;
function recognizeModificator(modif: string){
  switch(modif){
    case '*': return EventModificator.ZeroOrMore;
    case '+': return EventModificator.OneOrMore;
    default: return EventModificator.None;
  }
}
function parseEventQueryValue(input: string): EventQueryValue{
  const splited = split(input, /(\*|\+)/g);
  return {
    name: splited[0],
    modificator: recognizeModificator(splited[1])
  }
}
function parseEventQueryValues(input: string){
  return split(input, '|').map(parseEventQueryValue);
}
export class EventQuery  {
  public constructor(public readonly head: EventQueryValue[], public tail: EventQuery = null){

  }
  public isCurrentEvent(name: string){
    return this.head.some(x=>x.name===name)
  }
  public isNextEvent(name: string){
    return this.tail? this.tail.isCurrentEvent(name) : false;
  }
  /**
   * return this if event was declared with modificator OneOrMore(+) or ZeroOrMore(*), tail otherwise
   * @param firedEvent name of declared event
   */
  public next(firedEvent: string){
    return this.isRepeatableEvent(firedEvent) ? this : this.tail;
  }
  /**
   * find last EventQuery object going recursivle through
   */
  public last(){
    return this.tail?this.tail.last():this;
  }
  /**
   * check if event was declared with modificator OneOrMore(+) or ZeroOrMore(*)
   * @param event string value declared in query
   */
  public isRepeatableEvent(event: string){
    return this.head.some(x=>x.name==event && isRepeatableModificator(x.modificator))
  }
  public static toQueryObject(query: string) : EventQuery {
    const queryChain = split(query, '>');
    const head = parseEventQueryValues(queryChain[0]);
    const tail = queryChain.length > 1 ? EventQuery.toQueryObject(queryChain.slice(1).join('|')):null;
    return new EventQuery(head, tail);
  }
};

