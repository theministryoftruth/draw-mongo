import { EventQuery, EventQueryValue } from './QueryObject';

import { GestureListener } from './gesture-listener.model';
import { EventListenable } from './event-listenable.model';
export class Gesture{
  private static storage: Map<string, Gesture> = new Map();
  private query: EventQuery;
  public constructor(public readonly name: string, eventQuery: string){
    this.query = EventQuery.toQueryObject(eventQuery);
  }
  public listen(target: EventListenable){
    const listener = new GestureListener(this.query);
    this.listenToEventGroup(listener, target, this.query.head)
    return listener.emitter;
  }
  private createHandler(listener: GestureListener, target: EventListenable, eventName: string) {
    return {
      handler: (eventValue) => this.onEventFired(target, listener, eventName, eventValue),
      eventName
    }
  }

  private onEventFired(target: EventListenable, listener: GestureListener, firedEventName: string, eventValue: Event): void {
    const nextEventsListenTo = listener.check(firedEventName, eventValue).head;
    this.removeListeners(target, listener);
    this.listenToEventGroup(listener, target, nextEventsListenTo)
  }
  private removeListeners(target: EventListenable, listener: GestureListener) {
    listener.eventHandlers.forEach(x=>target.removeEventListener(x.eventName, x.handler));
  }

  private listenToEventGroup(listener: GestureListener, target: EventListenable, events: EventQueryValue[]){
    listener.eventHandlers = events.map(x=>{
      const handlerObj = this.createHandler(listener, target, x.name);
      target.addEventListener(handlerObj.eventName, handlerObj.handler)
      return handlerObj;
    })
  }
  public static register(name: string, gesture: Gesture){
    this.storage.set(name, gesture);
  }
  public static get(name: string){
    return this.storage.get(name);
  }
}
