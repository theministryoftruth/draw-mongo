import { GestureEventIndex } from './gesture-listener.model';
export class GestureEvent<T extends Event> extends Event{
  public constructor(readonly childEvent: T, type: string, readonly index: GestureEventIndex, readonly onAbortFn: ()=>void){
    super(type);
  }
  public abort(){
    this.onAbortFn();
  }
}
