import { DrawCommand } from '../drawing-compilation/commands/draw-command.model';

export interface Primitive {
  draw(): DrawCommand[];
}

