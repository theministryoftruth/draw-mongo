import { MergedObjects } from './merged-objects.model';
import { Chainable2D } from '../lib/basic/chainable.model';
import { Box2 } from '../geometry/box2.model';
import { Vector2 } from '../geometry/vector2.model';
import { flatten } from '@angular/compiler';
export class ChainOfObjects2D<T extends Chainable2D = Chainable2D> extends MergedObjects<T>{
  position: Vector2;
  public add(...list: T[]) {
    this.children = [...this.children, ...list];
    this.updatePositions();
  }
  protected updatePositions() {
    this.children.forEach((current, i) => {
      current.position = this.position;
      const previous = i == 0 ? null : this.children[i - 1];
      if (previous) {
        current.previous = previous;
        current.previous.next = current;
        current.previous.end = current.start;
      } else {
        current.previous = null;
      }
    });
  }
  public remove(...list: T[]) {
    this.children = this.children.filter(x => !list.includes(x));
    this.updatePositions();
  }
  public draw() {
    return flatten(this.map(x => x.draw()));
  }
  //TODO: add logic to resolve bounds of this object
  public bounds() {
    return Box2.Zero;
  }
}
