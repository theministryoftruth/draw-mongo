import { MergedObjects } from './merged-objects.model';
import { EventTarget2D } from '../lib/basic/event-target.model';
export class Hierarchy extends MergedObjects {
  public draw(){
    const output = []
    this.children.reduce((previous, current)=>{
      current.position = null;
      current.position = previous;
      output.push(...current.draw());
      return current.bounds();
    }, this.position || this.children[0].position);
    return output;
  }
  public add(...children: (EventTarget2D | Hierarchy)[]){
    children.forEach(child => this.addSingle(child))
  }
  private addSingle(child: Hierarchy | EventTarget2D) {
    if (child instanceof Hierarchy) {
      child.children.forEach(x=>x.parent = this)
      this.children.push(...child.children);
    }
    else {
      child.parent = this;
      this.children.push(child);
    }
  }
  public remove(...list: EventTarget2D[]){
    this.children = this.children.filter(x=>!list.includes(x));
  }
  public bounds(){
    return this.children[this.children.length-1].bounds();
  }
}
