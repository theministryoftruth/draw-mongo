import {Vector2} from '../geometry/vector2.model';
import {Box2} from '../geometry/box2.model';
import { MergedObjects } from './merged-objects.model';
import { EventTarget2D } from '../lib/basic/event-target.model';
export class Group extends MergedObjects{
  public add(...children: EventTarget2D[]){
    this.children.push(...children.map(child=>(child.parent = this,child)));
  }
  public remove(...list: EventTarget2D[]){
    this.children = this.children.filter(x=>!list.includes(x));
  }
  public draw(){
    return this.children.reduce((array, child)=>array.concat(child.draw()), []);
  }
  public bounds(){
    return this.getBounds(this.children.map(x=>x.bounds()));
  }
  private getMinBounds(bounds: Box2[]){
    if(bounds.length>0){
      const minX = bounds.reduce((prev, curr)=> Math.min(prev, curr.min.x), bounds[0].min.x);
      const minY = bounds.reduce((prev, curr)=> Math.min(prev, curr.min.y), bounds[0].min.y);
      return new Vector2(minX, minY);
    }
    else throw new Error('bounds array is empty')
  }
  private getMaxBounds(bounds: Box2[]){
     if(bounds.length>0){
      const maxX = bounds.reduce((prev, curr)=> Math.max(prev, curr.max.x), bounds[0].max.x);
      const maxY = bounds.reduce((prev, curr)=> Math.max(prev, curr.max.y), bounds[0].max.y);
      return new Vector2(maxX, maxY);
    }
    else throw new Error('bounds array is empty')
  }
  private getBounds(bounds: Box2[]){
    return new Box2(this.getMinBounds(bounds), this.getMaxBounds(bounds));
  }
}
