import { EventTarget2D } from '../lib/basic/event-target.model';

export abstract class MergedObjects <T extends EventTarget2D = EventTarget2D> extends EventTarget2D{

  protected children: T[] = [];
  public get count(){
    return this.children.length;
  }
  public abstract add(...list: T[]);
  public abstract remove(...list: T[]);
  protected onAnyEvent(name: string, value){
    this.children.forEach(x=>x.emit(name, value));
  }
  public checkStatus(state: string){
    return this.children.some(x=>x.checkStatus(state));
  }
  public get availableStatuses(){
    return this.children.reduce((prev, child)=> [...prev, ...child.availableStatuses.filter(state=>!prev.includes(state))], [])
  }
  public map<K>(mapFn: (item: T)=>K){
    return this.children.map(mapFn);
  }
  public filter<K>(filterFn: (item: T, index: number, array: T[])=>boolean){
    return this.children.filter(filterFn);
  }
}
