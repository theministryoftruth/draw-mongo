import { Primitive } from "../primitive-drawer.model";
import { Vector2 } from "../geometry/vector2.model";
import { LineDrawCommand } from "../../drawing-compilation/commands/line.draw-command.model";

export class Grid implements Primitive {
  public constructor(private originalSize: number, private color: string, public data: {
    zoomFactor: number,
    width: number,
    height: number,
    delta: Vector2
  }) {
  }
  public draw() {
    const size = this.originalSize * this.data.zoomFactor;
    const inRow = Math.ceil(this.data.width / size) + 2, inColumn = Math.ceil(this.data.height / size) + 2;
    const output = [];
    for (let i = - 1; i < inRow - 1; i++) {
      output.push(this.drawVerticalLine(i, size, this.data.height))
    }
    for (let i = -1; i < inColumn - 1; i++) {
      output.push(this.drawHorozintalLine(i, size, this.data.width))
    }
    return output;
  }

  private drawHorozintalLine(i: number, size: number, width: number) {
    let a =  this.generateVector(0, i * size+(this.data.delta.y % (size + 1))), b =  this.generateVector(width, i * size+(this.data.delta.y % (size + 1)));
    return this.createLineCommand(a, b, i);
  }

  private createLineCommand(a: Vector2, b: Vector2, i: number) {
    const line = new LineDrawCommand(this, a, b, { color: i % 5 == 0 ? this.color : "#ddd" });
    line.noWorldToScreenTransformation = true;
    return line;
  }

  private drawVerticalLine(i: number, size: number, heigth: number) {
    let a = this.generateVector(i * size+(this.data.delta.x % (size + 1)), 0), b =  this.generateVector(i * size+(this.data.delta.x % (size + 1)), heigth);
    return this.createLineCommand(a, b, i);
  }
  private generateVector(x: number, y: number){
   return new Vector2(x+0.5, y+0.5)
  }
}
