import { Vector2 } from '../../geometry/vector2.model';
import { Box2 } from '../../geometry/box2.model';
export type RectangleState = {
  bottomRight: Vector2,
  topLeft: Vector2
}
export function factoryRectangleState(data: Partial<RectangleState> = {}): RectangleState{
  return Object.assign({
    bottomRight: new Vector2(0,0),
    topLeft: new Vector2(0,0)
  }, data)
}
export function factoryRectangleStateFromBox(box: Box2){
  return factoryRectangleState({
    bottomRight: box.max,
    topLeft: box.min
  })
}
