import { Chainable2D } from "../../basic/chainable.model";
import { Vector2 } from "src/app/models/geometry/vector2.model";
import { LineDrawCommand } from '../../../../drawing-compilation/commands/line.draw-command.model';
import { Direction } from '../../../../utils/contants';
import { SectorDrawCommand } from "src/app/drawing-compilation/commands/sector.draw-command";

export class ArrowPart extends Chainable2D {
  public angleRadius: number = 3;
  public bounds() {
    return null;
  }
  public draw() {
    const start = this.position.add(this.start),
      end = this.position.add(this.end);
    const lineVector = this.end.substract(this.start);
    const shrinkVector = lineVector.normal().multiply(this.angleRadius);
    let output = [
      new LineDrawCommand(this, start.add(shrinkVector), end.substract(shrinkVector))
    ];
    if (this.isFirst) {
      output = [new LineDrawCommand(this,start,start.add(shrinkVector)), ...output]
    }
    if (this.isLast) {
      const leftWing = lineVector.rotate(Math.PI*7/8).normal().multiply(10);
      const rightWing = lineVector.rotate(-Math.PI*7/8).normal().multiply(10);
      output = [...output, new LineDrawCommand(this, end, end.substract(shrinkVector).add(leftWing)),
        new LineDrawCommand(this, end, end.substract(shrinkVector).add(rightWing))]
    } else {
      const currentAngle = lineVector.angleTo(Vector2.right);
      const sectorCmd = new SectorDrawCommand(this, end, this.angleRadius,
         this.angleRadius, 0, Math.PI*2, 0, {});
      output = [...output, sectorCmd]
    }
    return output;
  }
  public static fromPoint(a: Vector2, b: Vector2) {
    const part = new ArrowPart();
    part.start = a;
    part.end = b;
    return part;
  }
}
