import { Vector2 } from '../../geometry/vector2.model';
import { Box2 } from '../../geometry/box2.model';
import { TransformableObject2D } from '../basic/transformable.model';
import { factoryRectangleStateFromBox } from '../states/rectangle-state.model';
import { RectangleDrawCommand } from '../../../drawing-compilation/commands/rectangle.draw-command.model';
import { DrawStyle } from '../../../drawing-compilation/draw-style.model';

export class Rectangle extends TransformableObject2D{
  public constructor(public position: Vector2, public area: Box2, public style: DrawStyle){
    super();
    this.state = factoryRectangleStateFromBox(area);
  }
  public resizeByVector(vector: Vector2) {
    const max = this.state.bottomRight,
      min = this.state.topLeft;
    this.changeState({
      topLeft: new Vector2(min.x-vector.x, min.y+vector.y),
      bottomRight: new Vector2(max.x + vector.x, max.y-vector.y)
    });
  }
  public draw(){
    const {bottomRight, topLeft} = this.state;
    const bounds = this.getBoxWorldPosition(new Box2(bottomRight, topLeft));
    return [
      new RectangleDrawCommand(this, bounds, this.style)
    ]
  }
  public bounds(){
    return new Box2(this.state.bottomRight, this.state.topLeft).add(this.position);
  }
}
