import { Vector2 } from '../geometry/vector2.model';
import { Box2 } from "../geometry/box2.model";
import { Hierarchy } from '../merged-objects/hierarchy.model';
import { EventTarget2D } from './basic/event-target.model';
import { CuttedBox2 } from '../geometry/cutted-box2.model';
import { Direction } from '../../utils/contants';
import { EnumUtil } from '../../utils/enum.util';
import { RectangleDrawCommand } from '../../drawing-compilation/commands/rectangle.draw-command.model';
import { EllipseDrawCommand } from '../../drawing-compilation/commands/ellipse.draw-command.model';
import { factoryRectangleStateFromBox } from './states/rectangle-state.model';
import { Object2D } from '../geometry/object2d.model';

class TransformationRectangle extends EventTarget2D {
  position : Vector2 | Box2 =  new Vector2(0, 0);
  config: {
    resizeCircleRadius: number,
    reactCircleRadius: number,
    size: number
  } = {
    resizeCircleRadius: 5,
    reactCircleRadius: 15,
    size: 50
  }
  private resizeArea: CuttedBox2;
  private areaBox: Box2;
  public constructor() {
    super();
    const size = this.config.size;
    this.areaBox = new Box2(new Vector2(size, size), new Vector2(-size, -size));
    this.state = factoryRectangleStateFromBox(this.areaBox);
    const circleRadius = this.config.reactCircleRadius;
    this.resizeArea = new CuttedBox2(this.areaBox.max.add(circleRadius), this.areaBox.min.substract(circleRadius),this.areaBox);
  }
  public resizeByVector(vector: Vector2) {

  }
  public draw() {
    const {topLeft, bottomRight} = this.state;
    const bounds = this.getBoxWorldPosition(new Box2(topLeft, bottomRight));
    const circleRadius = this.config.resizeCircleRadius;
    return [
      new RectangleDrawCommand(this, bounds, { dashed: [5, 3], color: '#00f' }),
      new EllipseDrawCommand(this, bounds.min, circleRadius, circleRadius, { color: '#00d', fill: true, borderColor: 'rgb(0,148,158)' }),
      new EllipseDrawCommand(this, bounds.max, circleRadius, circleRadius, { color: '#00d', fill: true, borderColor: 'rgb(0,148,158)' }),
      new EllipseDrawCommand(this, new Vector2(bounds.min.x, bounds.max.y), circleRadius, circleRadius, { color: '#00d', fill: true, borderColor: 'rgb(0,148,158)' }),
      new EllipseDrawCommand(this, new Vector2(bounds.max.x, bounds.min.y), circleRadius, circleRadius, { color: '#00d', fill: true, borderColor: 'rgb(0,148,158)' }),
    ];
  }
  public onResizeBounds(point: Vector2){

    return this.resizeArea.add(this.position).contains(point);
  }
  public bounds() {
    const circleRadius = this.config.reactCircleRadius;
    return this.areaBox.add(this.position).shrink(circleRadius);
  }
  public getPointPositionRelativeToInternalBox(point: Vector2): Direction{
    const center = this.position instanceof Vector2? this.position : this.position.center;
    const x = point.x, y = point.y;
    return(x < center.x ? Direction.LEFT : Direction.RIGHT) | (y < center.y ? Direction.UP : Direction.DOWN);
  }
}
export class TransformationControl extends Hierarchy {
  public constructor() {
    super();
    this.children.push(new TransformationRectangle());
    this.event('drag').subscribe((event:{from: Vector2, to: Vector2})=>{
      this.onDragEventHandler(event.from, event.to);
    })
  }
  public get rectangle() {
    const top = this.children[this.children.length - 1];
    if (top instanceof TransformationRectangle) {
      return top;
    }
    else throw new Error("Transformation Control was corrupted");
  }
  public get transformable(){
    return this.children[0];
  }
  public add(...children: EventTarget2D[]) {
    const trRect = this.children.pop();
    if (trRect instanceof TransformationRectangle) {
      super.add(...children, trRect);
    }
    else {
      throw new Error("Transformation Control was corrupted")
    }
  }
  protected onDragEventHandler(start: Vector2, to: Vector2){
    const touchedResize = this.rectangle.onResizeBounds(start);
    if(touchedResize){
      const delta = this.resolveResizeDelta(start, to);
      this.transformable.emit('resize', delta);
    } else {
      const transformable = this.transformable;
      const delta = to.substract(start);
      transformable.position = (<any>transformable.position).add(delta);
    }
  }
  protected resolveResizeDelta(start: Vector2, to: Vector2){
    const defaultDelta = to.substract(start);
    const pointDirection = this.rectangle.getPointPositionRelativeToInternalBox(start);
    const koefX = EnumUtil.bitCheckAnd(pointDirection, Direction.LEFT) ? -1 : 1,
      koefY = EnumUtil.bitCheckAnd(pointDirection, Direction.DOWN) ? -1 : 1;
    return new Vector2(koefX*defaultDelta.x, koefY*defaultDelta.y);
  }
}
