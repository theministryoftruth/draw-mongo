import { EventTarget2D } from "./event-target.model";
import { GLOBAL_STATES_DICTIONARY } from './global-states.constant';
import { Vector2 } from '../../geometry/vector2.model';

export abstract class TransformableObject2D extends EventTarget2D{
  public get availableStatuses() { return  [GLOBAL_STATES_DICTIONARY.IDLE, GLOBAL_STATES_DICTIONARY.EDIT]; }
  public constructor(){
    super();
    this.event('click').subscribe(()=>this.switchEditMode(true));
    this.event('outclick').subscribe(()=>this.switchEditMode(false));
    this.event('idle');
    this.event('edit');
    this.event('resize').subscribe((delta:Vector2) => {
      this.resizeByVector(delta);
    })
  }
  public abstract resizeByVector(vector: Vector2);
  private switchEditMode(value) {
    return value? this.goto('edit') : this.goto('idle');
  }
}
