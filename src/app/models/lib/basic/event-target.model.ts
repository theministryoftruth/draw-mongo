import { Object2D } from "../../geometry/object2d.model";
import { EventEmitter } from "@angular/core";
import { GLOBAL_STATES_DICTIONARY } from './global-states.constant';
import { MergedObjects } from '../../merged-objects/merged-objects.model';

export abstract class EventTarget2D extends Object2D{
  private eventMap: Map<string, EventEmitter<any>> = new Map();
  private currentStatus: string = 'idle';
  private previousStatus: string = 'idle';
  private _parent: MergedObjects;
  public get parent(){
    return this._parent;
  }
  public set parent(newParent: MergedObjects){
    if(this._parent){
      this._parent.remove(this);
    }
    this._parent = newParent;
  }
  public event<T>(name: string){
    if(!this.eventMap.has(name)){
      const emitter = new EventEmitter<T>();
      this.eventMap.set(name, emitter);
      return emitter;
    }
    else throw new Error(`Event with the name ${name} has been already declared`);
  }
  public on(name: string, listener: (data: any, unsubscribe: ()=>void)=>void){
    const event = this.eventMap.get(name);
    if(event){
      const subscription = event.subscribe(data=>{
        listener(data, ()=>{
          if(subscription){
            subscription.unsubscribe()
          }
        })
      });
    }
    else console.warn(`Event with the name ${name} was not found`);
  }
  public emit(name: string, value?: any){
    if(this.eventMap.has(name)){
      this.eventMap.get(name).emit(value);
    }
    else this.onAnyEvent(name, value);
  }
  protected onAnyEvent(name: string, value){

  }
  public goto(stateKeyword: string){
    if(this.currentStatus!=stateKeyword) {
      this.previousStatus = this.currentStatus;
      this.currentStatus = stateKeyword;
      this.emit(this.currentStatus,this.previousStatus);
    } else {
      console.warn(`Somebody tries to set state ${stateKeyword} twice...`)
    }
  }
  protected restoreStatus(){
    const oldState = this.currentStatus;
    this.currentStatus = this.previousStatus;
    this.emit(this.currentStatus, oldState)
  }
  /**
   * checks if current status equals to stateKeyword. This method is supposed to override in subclasses to implement more complex logic
   * @param stateKeyword
   */
  public checkStatus(stateKeyword: string){
    return this.currentStatus == stateKeyword;
  }
  /**
   * return list of status which have affect for this object
   */
  public get availableStatuses(): string[] {
    return [GLOBAL_STATES_DICTIONARY.IDLE]
  }
}
