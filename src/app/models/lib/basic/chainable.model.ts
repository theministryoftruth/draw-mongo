import { Vector2 } from '../../geometry/vector2.model';
import { EventTarget2D } from './event-target.model';
export abstract class Chainable2D extends EventTarget2D {
  public position: Vector2;
  // TODO: maybe add Box2 also as parameter?
  public start: Vector2;
  public end: Vector2;
  public previous: Chainable2D = null;
  public next: Chainable2D = null;
  public get isLast(){
    return this.next === null;
  }
  public get isFirst(){
    return this.previous === null;
  }
}
