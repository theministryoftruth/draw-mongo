import { Group } from "../../merged-objects/group.model";
import { EventTarget2D } from './event-target.model';
import { Primitive } from "../../primitive-drawer.model";

export class Scene extends Group{
  public otherPrimitives: Primitive[] = [];
  public addPrimitives(...primitives: Primitive[]){
    this.otherPrimitives.push(...primitives);
  }
  public removePrimitives(...primitives: Primitive[]){
    this.otherPrimitives = this.otherPrimitives.filter(x=>!primitives.includes(x));
  }
}
