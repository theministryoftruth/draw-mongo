import { ObjectManager } from '../../services/object-manager.service';
import { EventTarget2D } from '../lib/basic/event-target.model';
import { Group } from '../merged-objects/group.model';
export abstract class ObjectManagerPluginBase{
  public constructor(protected manager: ObjectManager){

  }
  public onEventInit(){}
  public process(sceneRef: Group, object: EventTarget2D){ }
}
export const PluginToken = 'OBJECT_MANAGER_PLUGIN_TOKEN'
