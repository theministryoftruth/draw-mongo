import { ObjectManagerPluginBase } from '../object-manager-base.model';
import { Gesture } from '../../controls/gesture.model';
import { TransformableObject2D } from '../../lib/basic/transformable.model';
import { Vector2 } from '../../geometry/vector2.model';
import { GLOBAL_STATES_DICTIONARY } from '../../lib/basic/global-states.constant';
import { EventTarget2D } from '../../lib/basic/event-target.model';
import { Scene } from '../../lib/basic/scene.model';
import { TransformationControl } from '../../lib/transformation-rectangle.model';
import { ObjectManager } from '../../../services/object-manager.service';
import { GestureEvent } from '../../controls/gesture-event.model';
import { EventListenerPriority } from 'src/app/services/event.service';
export class TransformPlugin extends ObjectManagerPluginBase {

  private objectsToResize: TransformableObject2D[] = [];
  private resizeStartMousePoint: Vector2;
  private currentEditableControl: TransformationControl = null;
  public onEventInit() {
    const manager = this.manager;
    manager.events.addGestureListener('mousedrag', (eventData: GestureEvent<MouseEvent>) => {
      const mouse = manager.controls.mouseWorldPosition(eventData.childEvent);
      switch (eventData.type) {
        case 'mousedown':
          return this.resizeStageMouseDown(manager, mouse);
        case 'mousemove':
          if (this.resizeStartMousePoint) {
            this.resizeStageMouseMove(mouse);
            return false;
          } else {
            return true;
          }
        case 'mouseup':
          if (this.resizeStartMousePoint) {
            this.resizeStageMouseUp();
            return false;
          } else {
            return true;
          }
      }
    }, EventListenerPriority.High);
  }

  private resizeStageMouseUp() {
    this.resizeStartMousePoint = null;
  }

  private resizeStageMouseMove(mouse: Vector2) {
    this.currentEditableControl.emit('drag', {
      from: this.resizeStartMousePoint,
      to: mouse
    });
    this.resizeStartMousePoint = mouse;
  }

  private resizeStageMouseDown(manager: ObjectManager, mouse: Vector2) {
    const isRectangleSelected = this.currentEditableControl && manager.getObjectsBy(mouse, [this.currentEditableControl])[0] === this.currentEditableControl;
    if (isRectangleSelected) {
      this.resizeStartMousePoint = mouse;
      return false;
    }
    else {
      return true;
    }
  }
  public process(sceneRef: Scene, object: EventTarget2D) {
    if (object instanceof TransformableObject2D) {
      object.on('edit', () => {
        this.asEditable(this.manager, object, sceneRef);
      })
    }
  }

  private asEditable(manager: ObjectManager, object: EventTarget2D, sceneRef: Scene) {
    if (this.currentEditableControl !== null && this.currentEditableControl.transformable !== object) {
      this.currentEditableControl.transformable.goto(GLOBAL_STATES_DICTIONARY.IDLE);
    }
    this.currentEditableControl = manager.primitives.withControl(object);
    sceneRef.add(this.currentEditableControl);
    object.on('idle', (data, unsubscribe) => {
      this.asIdle(sceneRef, object);
      unsubscribe();
    });
  }

  private asIdle(sceneRef: Scene, object: EventTarget2D) {
    sceneRef.add(object);
    sceneRef.remove(this.currentEditableControl);
    this.currentEditableControl = null;
  }
}
