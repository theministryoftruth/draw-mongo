import { Vector2 } from './vector2.model';
export class Box2{
  private cachedAB: Vector2;
  private cachedAD: Vector2;
  public readonly center: Vector2;
  public constructor(readonly max: Vector2, readonly min: Vector2){
    this.cachedAB = new Vector2(max.x, min.y).substract(min);
    this.cachedAD = new Vector2(min.x,max.y).substract(min);
    this.center = new Vector2((max.x+min.x)/2,(max.y+min.y)/2)
  }
  public add(v: Vector2 | Box2){
    if(v instanceof Vector2){
      return new Box2(this.max.add(v), this.min.add(v))
    }
    else return new Box2(this.max.add(v.max), this.min.add(v.min))
  }
  public shrink(n: number){
    return new Box2(this.max.add(n), this.min.substract(n));
  }
  public multiply(v: number){
    return new Box2(this.max.multiply(v), this.min.multiply(v));
  }
  /**
   * see mathimatical stackoverflow for the explanation. IDK what I have done
   * @link https://math.stackexchange.com/questions/190111/how-to-check-if-a-point-is-inside-a-rectangle#answer-190373
   */
  public contains(point: Vector2){
    const pointA = point.substract(this.min),
      AMAB = pointA.dot(this.cachedAB),
      AMAD = pointA.dot(this.cachedAD);
    return 0 < AMAB && AMAB < this.cachedAB.length*this.cachedAB.length && 0 < AMAD && AMAD < this.cachedAD.length*this.cachedAD.length;
  }
  public static Zero = new Box2(new Vector2(0,0), new Vector2(0,0));
  public static factory(maxX: number, maxY: number, minX:number, minY: number){
    return new Box2(new Vector2(maxX, maxY), new Vector2(minX, minY));
  }
}
