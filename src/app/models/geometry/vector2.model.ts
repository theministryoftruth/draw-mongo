import { Direction } from '../../utils/contants';

export class Vector2 {
  public _length: number;
  public constructor(public x: number, public y: number) {
  }
  public add(v: number | Vector2) {
    if (typeof v == 'number') {
      return new Vector2(this.x + v, this.y + v);
    }
    else return new Vector2(this.x + v.x, this.y + v.y);
  }
  public multiply(v: number | Vector2) {
    if (typeof v == 'number') {
      return new Vector2(this.x * v, this.y * v);
    } else {
      return new Vector2(this.x * v.x, this.y * v.y)
    }
  }
  public get length() {
    if (this._length === undefined) {
      this._length = Math.sqrt(this.dot(this))
    }
    return this._length;
  }
  public dot(v: Vector2) {
    return this.x * v.x + this.y * v.y;
  }
  public substract(v: number | Vector2) {
    if (typeof v == 'number') {
      return new Vector2(this.x - v, this.y - v);
    }
    else return new Vector2(this.x - v.x, this.y - v.y);
  }
  public equal(b: Vector2) {
    return this.x === b.x && this.y === b.y;
  }
  public normal() {
    return new Vector2(this.x / this.length, this.y / this.length);
  }
  public moveToward(direction: Direction, distance: number) {
    const normal = Vector2.getByDirection(direction);
    return this.add(normal.multiply(distance));
  }
  public product(other: Vector2) {
    return this.x * other.y - this.y * other.x;
  }
  public angleTo(other: Vector2) {
    const dot = this.dot(other);
    const det = this.product(other);
    return Math.atan2(det, dot);
  }
  public rotate(angle: number){
    const angleSin = Math.sin(angle), angleCos = Math.cos(angle);
    return new Vector2(this.x*angleCos+this.y*angleSin, -this.x*angleSin + this.y*angleCos)
  }
  public static readonly up = new Vector2(0, -1);
  public static readonly down = new Vector2(0, 1);
  public static readonly left = new Vector2(-1, 0);
  public static readonly right = new Vector2(1, 0);
  public static readonly zero = new Vector2(0, 0);
  public static getByDirection(direction: Direction) {
    switch (direction) {
      case Direction.LEFT: return Vector2.left;
      case Direction.UP: return Vector2.up;
      case Direction.RIGHT: return Vector2.right;
      case Direction.DOWN: return Vector2.down;
      default: return new Vector2(0, 0);
    }
  }

}
