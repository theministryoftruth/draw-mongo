import { Box2 } from './box2.model';
import { Vector2 } from './vector2.model';
export class CuttedBox2 extends Box2{
  public constructor(readonly max: Vector2, readonly min: Vector2, readonly internalBox: Box2){
    super(max, min);
  }
  public contains(point: Vector2){
    return super.contains(point) && !this.internalBox.contains(point);
  }
  public add(n: Box2 | Vector2){
    const total = super.add(n);
    const internal = this.internalBox.add(n);
    return new CuttedBox2(total.max, total.min, internal);
  }
}
