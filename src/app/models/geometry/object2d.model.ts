import { Primitive } from '../primitive-drawer.model';
import { Vector2 } from './vector2.model';
import { Box2 } from './box2.model';
import { DrawCommand } from '../../drawing-compilation/commands/draw-command.model';
export abstract class Object2D implements Primitive {
  public position: Vector2 | Box2;
  protected state: any;
  public abstract bounds(): Box2;
  /**
   * Transform vector coordinates from local space to world space
   * @param vec vector to transform
   */
  public getWorldPosition(vec: Vector2) {
    if (this.position instanceof Vector2) {
      return vec.add(this.position);
    }
    else {
      let outX = vec.x + (vec.x < 0 ? this.position.min.x : this.position.max.x),
        outY = vec.y + (vec.y < 0 ? this.position.min.y : this.position.max.y);
      return new Vector2(outX, outY);
    }
  }
  public getBoxWorldPosition(box: Box2){
    return new Box2(this.getWorldPosition(box.max), this.getWorldPosition(box.min))
  }
  /**
   * returns a list of draw commands
   */
  public abstract draw(): DrawCommand[];
  /**
   * change internal state of Object2D
   * @param statePart partial state object
   */
  public changeState(statePart){
    this.state = Object.assign({}, this.state, statePart);
  }

}
