import { Vector2 } from "./vector2.model";

export class VectorPath2D implements Iterable<Vector2> {
  *[Symbol.iterator](): Iterator<Vector2> {
    for(const vector of this.points){
      yield vector;
    }
  }
  public constructor(private points: Vector2[]){

  }
  public get start(){
    return this.points[0];
  }
}
