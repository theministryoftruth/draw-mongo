
import { Injectable, Inject } from "@angular/core";
import { Vector2 } from '../models/geometry/vector2.model';
import { Box2 } from '../models/geometry/box2.model';
import { Rectangle } from "../models/lib/std-objects/rectangle.model";
import { Grid } from "../models/lib/grid.model";
import { EventTarget2D } from "../models/lib/basic/event-target.model";
import { TransformationControl } from '../models/lib/transformation-rectangle.model';
import { ViewControlService } from './view-control.service';
import { CanvasService } from "./canvas.service";
import { ArrowService } from './arrows.service';
import { Direction } from '../utils/contants';

@Injectable()
export class Primitives2DFactory {
  public constructor(@Inject(ViewControlService) private view: ViewControlService,
    @Inject(CanvasService) private canvas: CanvasService,
    @Inject(ArrowService) private arrows: ArrowService){

  }
  public grid(size: number, color?: string) {
    let gridData = {
      delta: this.view.shift,
      width: this.canvas.element.width,
      height: this.canvas.element.height,
      zoomFactor: this.view.zoomFactor
    }
    this.canvas.canvasWillRerender.subscribe(()=>{
      gridData.delta = this.view.shift;
      gridData.width = this.canvas.element.width;
      gridData.height = this.canvas.element.height;
      gridData.zoomFactor = this.canvas.zoomFactor;
    })
    return new Grid(size, color, gridData);
  }
  public simpleRect(position: Vector2, bounds: Box2) {
    return new Rectangle(position, bounds, {fill: true});
  }
  public transformControl() {
    return new TransformationControl();
  }
  public withControl(object: EventTarget2D) {
    const control = this.transformControl();
    control.add(object);
    return control;
  }
  public arrow(start: Vector2, end: Vector2, startDirection: Direction, endDirection: Direction){
    return this.arrows.createArrow(start,end, startDirection, endDirection);
  }
}
