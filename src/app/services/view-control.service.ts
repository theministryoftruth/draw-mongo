import { Injectable, Inject, EventEmitter, NgZone } from "@angular/core";
import { fromEvent, Subscription, BehaviorSubject } from "rxjs";
import { CanvasService } from './canvas.service';
import { Vector2 } from '../models/geometry/vector2.model';
import { Gesture } from '../models/controls/gesture.model';
import { GestureEventIndex } from '../models/controls/gesture-listener.model';
import { WorldToScreenResolver } from '../drawing-compilation/commands/draw-command.model';
import { GestureEvent } from '../models/controls/gesture-event.model';
import { EventService, EventListenerPriority } from './event.service';
@Injectable()
export class ViewControlService {
  private lastMousePosition: Vector2;
  public cursorType = new BehaviorSubject<string>("default");
  private _shift: Vector2 = new Vector2(0, 0);
  private zoomValue: number = 1;
  public get shift(){
    return this._shift;
  }
  public get center(){
    if(this.canvas){
      return this.canvas.center;
    }
  }
  public get zoomFactor(){
    return this.zoomValue;
  }
  public constructor(@Inject(CanvasService) private canvas: CanvasService,
    @Inject(EventService) private events: EventService) {

  }
  public pseudoScalar(value: number) {
    return value * this.zoomValue;
  }
  public allowZooming() {
    this.events.addEventListener('wheel', (event: WheelEvent) => {
      const direction = Math.sign(event.deltaY);
      if (direction < 0) this.zoomValue *= 1.2;
      else this.zoomValue /= 1.2;
      this.canvas.zoomFactor = this.zoomValue;
      return true;
    }, EventListenerPriority.Low)
  }
  public allowSliding() {
    const onMouseUp = () => {
      this.changeCursor('default');
    };
    const onMouseMoving = (gesture: GestureEvent<MouseEvent>) => {
      const event = gesture.childEvent;
      const delta = new Vector2(event.clientX, event.clientY).substract(this.lastMousePosition);
      this.lastMousePosition = this.lastMousePosition.add(delta);
      this._shift = this._shift.add(delta.multiply(1 / this.zoomValue));
      this.canvas.trigger();
    }
    const onMouseDown = (event: GestureEvent<MouseEvent>) => {
      this.changeCursor('-webkit-grabbing');
      this.lastMousePosition = this.lastMousePosition = this.mousePosition(event.childEvent);
    }
    this.events.addGestureListener<MouseEvent>('mousedrag', event=>{
      switch(event.type){
        case 'mousedown':
          onMouseDown(event);
          break;
        case 'mousemove':
          onMouseMoving(event);
          break;
        case 'mouseup':
          onMouseUp();
          break;
      }
      return true;
    },  EventListenerPriority.Low);

  }
  public mousePosition(event: MouseEvent): Vector2 {
    return new Vector2(event.clientX, event.clientY);
  }
  public mouseWorldPosition(event: MouseEvent): Vector2 {
    const screenVector = this.mousePosition(event);
    return screenVector.substract(this.canvas.center).multiply(1/this.zoomValue).substract(this.shift);
  }

  private changeCursor(value: string) {
    requestAnimationFrame(() => this.cursorType.next(value))
  }
  public factoryWorldToScreenResolver(): WorldToScreenResolver{
    return {
      resolve: (worldVector) => {
        return worldVector
        .add(this.shift) //adding shift of the world
        .multiply(this.zoomValue) // multipling dimensions for correct zooming
        .add(this.center) // fixing coordinates zero point (default zero point for a canvas is the left top corner, but required the center of the canvas)
      },
      psedoScalar: this.pseudoScalar.bind(this)
    }
  }
}
