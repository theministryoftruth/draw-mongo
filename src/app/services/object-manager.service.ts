import { Injectable, Inject, NgZone, Optional } from "@angular/core";
import { CanvasService } from './canvas.service';
import { ViewControlService } from './view-control.service';
import { Primitive } from '../models/primitive-drawer.model';
import { Vector2 } from '../models/geometry/vector2.model';
import { Box2 } from "../models/geometry/box2.model";
import { Primitives2DFactory } from './primitive.factory';
import { EventTarget2D } from "../models/lib/basic/event-target.model";
import { PluginToken, ObjectManagerPluginBase } from '../models/plugins/object-manager-base.model';
import { Scene } from "../models/lib/basic/scene.model";
import { DrawingCompiler } from '../drawing-compilation/drawing-compiler.service';
import { EventService, EventListenerPriority } from "./event.service";

@Injectable()
export class ObjectManager {
  private plugins: ObjectManagerPluginBase[] = [];
  private scene: Scene = new Scene();
  public constructor(@Inject(CanvasService) public canvas: CanvasService,
    @Inject(ViewControlService) public controls: ViewControlService,
    @Inject(Primitives2DFactory) public primitives: Primitives2DFactory,
    @Inject(DrawingCompiler) private compiler: DrawingCompiler,
    @Inject(EventService) public events: EventService,
    @Inject(NgZone) private ngZone: NgZone,
    @Optional() @Inject(PluginToken) plugins: ({new (object: ObjectManager): ObjectManagerPluginBase})[]) {
      this.plugins = plugins.map(Class=>new Class(this));
  }
  public init(canvasRef: HTMLCanvasElement) {
    this.ngZone.runOutsideAngular(() => {
      this.canvas.initCanvas(canvasRef);
      this.controls.allowZooming();
      this.controls.allowSliding();
      this.plugins.forEach(x=>x.onEventInit());
      this.events.addEventListener('click', (event: MouseEvent)=>{
        this.onClick(this.controls.mousePosition(event));
        return true;
      }, EventListenerPriority.Low);
    })
  }
  public add(...objects: Primitive[]) {
    const grouped = objects.reduce((prev, curr) => {
      if (curr instanceof EventTarget2D) prev.objects2D.push(curr);
      else prev.other.push(curr);
      return prev;
    }, {
        objects2D: [],
        other: []
      })
    this.scene.add(...grouped.objects2D);
    grouped.objects2D.forEach(object => this.plugins.forEach(plugin=>plugin.process(this.scene, object)))
    this.scene.addPrimitives(...grouped.other);
  }
  public remove(primitive: Primitive){
    if(primitive instanceof EventTarget2D){
      this.scene.remove(primitive);
    } else {
      this.scene.removePrimitives(primitive);
    }
  }
  public render() {
    this.compiler.clearBuffer();
    this.scene.otherPrimitives.forEach(x=>this.compiler.addCommands(x.draw()));
    this.scene.map(x=>this.compiler.addCommands(x.draw()));
    this.compiler.compile();
  }
  protected onClick(mousePosition: Vector2) {
    this.scene.map(x => {
      const bounds = this.getBoundsScreenPosition(x.bounds());
      if (bounds.contains(mousePosition)) {
        x.emit('click')
      }
    })
    this.canvas.trigger();
  }
  public getObjectsBy(position: Vector2, source?: EventTarget2D[]){
    if(source) {
      return source.filter(x => x.bounds().contains(position))
    }
    else{
      return this.scene.filter(x => x.bounds().contains(position))
    }
  }
  protected getBoundsScreenPosition(box: Box2) {
    const data = this.controls;
    return box.add(data.shift)
      .multiply(data.zoomFactor)
      .add(data.center)
  }

}
