import { Injectable, Inject } from "@angular/core";
import { Vector2 } from '../models/geometry/vector2.model';
import { Direction, oppositeDirection } from "../utils/contants";
import { EnumUtil } from "../utils/enum.util";
import { VectorPath2D } from "../models/geometry/path2d.model";
import { ChainOfObjects2D } from "../models/merged-objects/object-chain.model";
import { ArrowPart } from "../models/lib/std-objects/arrows/arrow-part.model";
import { IteratorUtil } from "../utils/iterator.util";
import { HotkeyService, HotkeyEventType } from './hotkey.service';
import { EventService } from './event.service';
import { ViewControlService } from './view-control.service';
import { Subject } from "rxjs";

export type ArrowPathConstrain = {
  direction: Direction,
  distance: number
}
@Injectable()
export class ArrowService {
  private startArrowPosition: Vector2;
  public constructor(@Inject(HotkeyService) private hotkeys: HotkeyService,
    @Inject(EventService) private events: EventService,
    @Inject(ViewControlService) private view: ViewControlService) {

  }
  public createArrowPathBetween(start: Vector2, end: Vector2, startConstraint: ArrowPathConstrain, endConstraint: ArrowPathConstrain) {
    const definedStart = start.moveToward(startConstraint.direction, startConstraint.distance);
    const definedEnd = end.moveToward(oppositeDirection(endConstraint.direction), endConstraint.distance);
    const points = [start, ...this.getPathPointsList(definedStart, definedEnd, startConstraint.direction, endConstraint.direction), end];
    return new VectorPath2D(points);
  }
  private getPathPointsList(start: Vector2, end: Vector2, previousDirection: Direction, endDirectionConstraint: Direction) {
    if (start.equal(end)) {
      return [end];
    } else {
      const { direction, distance } = this.getNextDirectionAndDistance(previousDirection, end, start);
      const nextStart = start.moveToward(direction, distance);
      if (nextStart.equal(end) && oppositeDirection(direction) == endDirectionConstraint) {
        start = start.moveToward(oppositeDirection(previousDirection), 30);
        return this.getPathPointsList(start, end, previousDirection, endDirectionConstraint);
      }
      return [start, ...this.getPathPointsList(nextStart, end, direction, endDirectionConstraint)];
    }

  }
  private getNextDirectionAndDistance(previousDirection: Direction, end: Vector2, start: Vector2) {
    if (EnumUtil.bitCheckOr(previousDirection, Direction.LEFT | Direction.RIGHT)) {
      const dir = end.y > start.y ? Direction.DOWN : Direction.UP;
      return {
        direction: dir,
        distance: Math.abs(end.y - start.y)
      }
    }
    else {
      const dir = end.x > start.x ? Direction.RIGHT : Direction.LEFT;
      return {
        direction: dir,
        distance: Math.abs(end.x - start.x)
      }
    }
  }
  public createArrow(from: Vector2, to: Vector2, startDirection: Direction, endDirection: Direction) {
    const arrow = new ChainOfObjects2D<ArrowPart>();
    const path = this.createArrowPathBetween(Vector2.zero, to.substract(from), {
      direction: startDirection,
      distance: 30,
    }, {
        direction: endDirection,
        distance: 30
      });
    arrow.position = from;
    const fromPoints = IteratorUtil.continiousMap(ArrowPart.fromPoint);
    arrow.add(...fromPoints(path));
    return arrow;
  }
  allowHotArrows() {
    const observable = new Subject<ChainOfObjects2D<ArrowPart>>();
    this.hotkeys.key('Alt', HotkeyEventType.Up).subscribe(() => {
      const mouse = this.events.getLastMouseEvent();
      this.startArrowPosition = this.view.mouseWorldPosition(mouse);
      this.events.addEventListener('mousemove', (event: MouseEvent)=>{
        const currentMouse = this.view.mouseWorldPosition(event);
        if(currentMouse.substract(this.startArrowPosition).length > 30){
          observable.next(this.createArrow(this.startArrowPosition, currentMouse, Direction.RIGHT, Direction.RIGHT));
        }
        return true;
      })
    });
    return observable;
  }
}
