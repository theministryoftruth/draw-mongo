import { Injectable, Inject, EventEmitter, InjectionToken, Optional, NgZone } from '@angular/core';
import { CanvasService } from './canvas.service';
import { Gesture } from '../models/controls/gesture.model';
import { GestureEvent } from '../models/controls/gesture-event.model';
import { Observable } from 'rxjs';

export enum EventListenerPriority {
  High,
  Low
}
type BooleanEventListener<T extends Event = Event> = ((event: T) => boolean);
@Injectable()
export class EventService {
  private lastMouseEvent: MouseEvent;
  private eventMap: Map<string, EventEmitter<Event>> = new Map();
  private listenersMap: Map<string, BooleanEventListener[]> = new Map();
  public constructor(@Inject(CanvasService) private canvas: CanvasService,
    @Inject(NgZone) private ngZone: NgZone) {
    this.canvas.elementReady.then(() => {
      this.event('mousemove').subscribe((event: MouseEvent) => {
        this.lastMouseEvent = event;
      })
    })
  }
  event(eventName: string, globalEvent: boolean = false) {
    if (this.eventMap.has(eventName)) {
      return this.eventMap.get(eventName);
    } else {
      const emitter = new EventEmitter<Event>();
      this.getEventTarget(globalEvent).addEventListener(eventName, (event) => {
        emitter.emit(event);
      })
      this.eventMap.set(eventName, emitter);
      return emitter;
    }
  }
  private getEventTarget(globalEvent) {
    if(globalEvent){
      return document.body;
    } else {
      return this.canvas.element;
    }
  }

  gesture(eventName: string) {
    if (this.eventMap.has(eventName)) {
      return this.eventMap.get(eventName);
    } else {
      const requested = Gesture.get(eventName);
      const emitter = requested.listen(this);
      this.eventMap.set(eventName, emitter);
      return emitter;
    }
  }
  addGestureListener<T extends Event>(name: string, listener: BooleanEventListener<GestureEvent<T>>, priority: EventListenerPriority) {
    this.addListener(name, listener, priority, this.gesture(name));
  }
  addEventListener(name: string, listener: BooleanEventListener, priority: EventListenerPriority = EventListenerPriority.Low) {
    this.addListener(name, listener, priority, this.event(name));
  }
  removeEventListener(name: string, listener: BooleanEventListener) {
    const listenerList = this.listenersMap.get(name);
    if (listenerList) {
      const nextListenerList = listenerList.filter(x => x != listener);
      this.listenersMap.set(name, nextListenerList);
    }
  }
  private addListener(name: string, listener: BooleanEventListener, priority: EventListenerPriority, emitter: EventEmitter<Event>) {
    listener = this.safeCallback(listener);
    if (!this.listenersMap.has(name)) {
      this.listenersMap.set(name, [listener]);
      emitter.subscribe((event: Event) => {
        for (const listener of this.listenersMap.get(name)) {
          if (!listener(event)) {
            break;
          }
        }
        this.canvas.trigger();
      });
    }
    else {
      const oldListenerList = this.listenersMap.get(name);
      this.listenersMap.set(name, this.factoryNextListenerList(oldListenerList, priority, listener));
    }
  }
  public observeEvent(eventName: string, globalEvent = false) {
    const event = this.event(eventName, globalEvent);
    return new Observable((observer) => {
      event.subscribe(result => {
        observer.next(result);
      })
    })
  }
  private factoryNextListenerList(prevList: BooleanEventListener[], priority: EventListenerPriority, listener: BooleanEventListener) {
    switch (priority) {
      case EventListenerPriority.High:
        return [listener, ...prevList];
      case EventListenerPriority.Low:
        return [...prevList, listener];
    }
  }

  private safeCallback(cb: (event: Event) => boolean) {
    return (event: Event) => this.ngZone.runOutsideAngular(() => cb(event));
  }
  getLastMouseEvent() {
    return  this.lastMouseEvent;
  }
}
