import { Inject, EventEmitter } from "@angular/core";
import { EventService, EventListenerPriority } from './event.service';
import {filter} from 'rxjs/operators';
import { StringUtils } from '../utils/string.util';

export enum HotkeyEventType{
  Down,
  Up,
  Press
}

export class HotkeyService{
  public constructor(@Inject(EventService) private events: EventService){

  }
  public key(keyName: string, type: HotkeyEventType){
    return this.events.observeEvent(this.getKeyEventByType(type), true).pipe(
      this.getObservableFilterByKeyName(keyName)
    );
  }
  private getObservableFilterByKeyName(keyName: string){
    const splited = keyName.split(/\+/g);
    const altRequired = splited.length > 1 && StringUtils.caseInsensetiveSome(splited, 'alt');
    const ctrlRequired = splited.length > 1 && StringUtils.caseInsensetiveSome(splited, 'ctrl');
    const shiftRequired = splited.length > 1 && StringUtils.caseInsensetiveSome(splited, 'shift');
    const exactKey = splited.length == 1? splited[0] : splited.find(x=>{
      const upper = x.toUpperCase();
      return upper!='ALT'&&upper!='CTRL'&&upper!='SHIFT';
    })
    return filter((event: KeyboardEvent)=>{
      const key = event.key;
      let keyMatch = exactKey && key && key.toUpperCase()===exactKey.toUpperCase();
      return altRequired == event.altKey
        && ctrlRequired == event.ctrlKey
        && shiftRequired == event.shiftKey
        && keyMatch
    })
  }
  private getKeyEventByType(type: HotkeyEventType){
    switch(type){
      case HotkeyEventType.Down: return 'keydown';
      case HotkeyEventType.Up: return 'keyup';
      case HotkeyEventType.Press: return 'keypress';
    }
  }
}
