import { Injectable, EventEmitter } from '@angular/core';
import { Vector2 } from '../models/geometry/vector2.model';
import { first } from 'rxjs/operators';

@Injectable()
export class CanvasService {
  private canvasElement: HTMLCanvasElement;
  public get element(){
    return this.canvasElement;
  }
  private drawCtx: CanvasRenderingContext2D;
  public get context(){
    return this.drawCtx || (this.element && this.element.getContext("2d"))
  }
  public center: Vector2;
  public zoomFactor = 1;
  public shift: Vector2 = new Vector2(0,0);
  public initCanvas(canvas: HTMLCanvasElement) {
    this.setCanvasSize(canvas);
    this.canvasElement = canvas;
    this.drawCtx = canvas.getContext("2d");
    this.center = new Vector2(document.documentElement.clientWidth/2,  document.documentElement.clientHeight/2);
    /*window.addEventListener('resize', () => {
      this.setCanvasSize(canvas);
      this.trigger();
    })*/
    this.trigger()
  }
  private setCanvasSize(canvas: HTMLCanvasElement) {
    const width = document.documentElement.clientWidth, height = document.documentElement.clientHeight;
    canvas.width = width;
    canvas.height = height;
  }
  public trigger(){
    this.canvasWillRerender.emit();
  }
  public canvasWillRerender = new EventEmitter<void>();
  public elementReady: Promise<void> = this.canvasWillRerender.pipe(first()).toPromise() ;
  public background(color: string) {
    this.drawCtx.fillStyle = color;
    this.drawCtx.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
  }
}
