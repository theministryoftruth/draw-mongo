export class DrawStream {
  private static streams: Map<string, DrawStream> = new Map();
  public constructor(private name: string){
    DrawStream.streams.set(name, this);
  }
  public static open(name: string, mode: DrawStreamOpenMode){
    switch(mode){
      case DrawStreamOpenMode.OpenOrCreate:
        if(DrawStream.streams.has(name))
          return DrawStream.streams.get(name);
      case DrawStreamOpenMode.Create:
        return new DrawStream(name);
      case DrawStreamOpenMode.Open:
        if(DrawStream.streams.has(name))
          return DrawStream.streams.get(name);
        else throw new Error(`Draw stream by name ${name} was not found`);
    }
  }
  private static register(name: string){

  }
}
export enum DrawStreamOpenMode {
  Create,
  OpenOrCreate,
  Open
}
