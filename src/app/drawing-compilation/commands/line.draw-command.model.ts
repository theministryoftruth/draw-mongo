import { DrawCommand } from "./draw-command.model";
import { Primitive } from '../../models/primitive-drawer.model';
import { Vector2 } from "../../models/geometry/vector2.model";
import { DrawStyle } from "../draw-style.model";

export class LineDrawCommand extends DrawCommand{
  public constructor(sender: Primitive, protected a: Vector2, protected b: Vector2, protected style: DrawStyle = {}){
    super(sender);
  }
  public execute(ctx: CanvasRenderingContext2D){
    let a = this.worldToScreenResolver.resolve(this.a), b = this.worldToScreenResolver.resolve(this.b);
    ctx.save()
    ctx.beginPath();
    if (this.style.adjust) {
      a = a.add(this.style.adjust);
      b = b.add(this.style.adjust);
    }
    ctx.moveTo(a.x, a.y);
    ctx.lineTo(b.x, b.y);
    this.stroke(ctx, this.style.linewidth, this.style.color);
    ctx.restore();
  }
}
