import { DrawCommand } from '../draw-command.model';
export abstract class DrawCommandMetadata extends DrawCommand {
  public constructor(sender, readonly command: DrawCommand){
    super(sender);
  }
  public execute(ctx: CanvasRenderingContext2D){
    this.applyMetadata();
    return this.command.execute(ctx);
  }
  protected abstract applyMetadata();
}
