import { DrawCommandMetadata } from './draw-command-metadata.model';
export class PreventWorldToScreenMetadata extends DrawCommandMetadata{
  protected applyMetadata() {
    this.command.noWorldToScreenTransformation = true;
  }
}
