import { Primitive } from "../../models/primitive-drawer.model";
import { DrawStyle } from "../draw-style.model";
import { Box2 } from "../../models/geometry/box2.model";
import { DrawCommand } from "./draw-command.model";
export class RectangleDrawCommand extends DrawCommand {
  public constructor(sender: Primitive, protected bounds: Box2, protected style: DrawStyle = {}) {
    super(sender);
  }
  public execute(ctx: CanvasRenderingContext2D) {
    const style = this.style;
    const bounds = new Box2(this.worldToScreenResolver.resolve(this.bounds.max), this.worldToScreenResolver.resolve(this.bounds.min));
    ctx.save();
    if (style.fill) {
      this.fill(ctx, style.color, bounds);
    }
    else {
      if (style.dashed && style.dashed.length > 0) {
        ctx.setLineDash(style.dashed);
      }
      this.stroke(ctx, style.linewidth, style.color, bounds);
    }
    ctx.restore();
  }
}
