import { Primitive } from "../../models/primitive-drawer.model";
import { DrawStyle } from "../draw-style.model";
import { Vector2 } from '../../models/geometry/vector2.model';
import { DrawCommand } from "./draw-command.model";
import { SectorDrawCommand } from "./sector.draw-command";
export class EllipseDrawCommand extends SectorDrawCommand {
  public constructor(sender: Primitive, center: Vector2, radiusX: number, radiusY: number, style: DrawStyle) {
    super(sender, center, radiusX, radiusY, 0, 2 * Math.PI, 0, style);
  }
}
