import { DrawCommand } from "./draw-command.model";
import { Primitive } from "src/app/models/primitive-drawer.model";
import { Vector2 } from "src/app/models/geometry/vector2.model";
import { DrawStyle } from "../draw-style.model";

export class SectorDrawCommand extends DrawCommand {
  public constructor(sender: Primitive,
    private center: Vector2,
    private radiusX: number,
    private radiusY: number,
    private startAngle: number,
    private endAngle: number,
    private rotation: number,
    private style: DrawStyle = {},
    ) {
    super(sender);
  }
  public execute(ctx: CanvasRenderingContext2D) {
    const center = this.worldToScreenResolver.resolve(this.center);
    const radiusX =  this.worldToScreenResolver.psedoScalar(this.radiusX);
    const radiusY = this.worldToScreenResolver.psedoScalar(this.radiusY);
    ctx.save();
    ctx.beginPath();
    ctx.ellipse(center.x, center.y, radiusX, radiusY, this.rotation, this.startAngle, this.endAngle, false);
    if (this.style.fill) {
      this.fill(ctx, this.style.color);
      if (this.style.borderColor) {
        ctx.beginPath();
        ctx.ellipse(center.x, center.y, radiusX, radiusY, this.rotation, this.startAngle, this.endAngle, false);
        this.stroke(ctx, 2, this.style.borderColor);
      }
    }
    else {
      if (this.style.dashed && this.style.dashed.length > 0) {
        ctx.setLineDash(this.style.dashed);
      }
      this.stroke(ctx, this.style.linewidth, this.style.color);
    }
    ctx.restore();
  }
}
