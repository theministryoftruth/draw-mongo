import { Primitive } from "../../models/primitive-drawer.model";
import { Box2 } from "../../models/geometry/box2.model";
import { Object2D } from "../../models/geometry/object2d.model";
import { Vector2 } from '../../models/geometry/vector2.model';

export interface WorldToScreenResolver{
  resolve(vector: Vector2): Vector2;
  psedoScalar(scalar: number):number;
}
export abstract class DrawCommand {
  public constructor(public readonly sender: Primitive){

  };
  private _internalResolver: WorldToScreenResolver;
  public get worldToScreenResolver(): WorldToScreenResolver{
    return this._internalResolver;
  }
  private _assignedResolver: WorldToScreenResolver;
  public noWorldToScreenTransformation = false;
  public set worldToScreenResolver(value: WorldToScreenResolver){
    this._assignedResolver = value;
    this._internalResolver = {
      resolve: (vector)=> {
        if(this.noWorldToScreenTransformation){
          return vector;
        } else {
          return this._assignedResolver.resolve(vector);
        }
      },
      psedoScalar: number => {
        if(this.noWorldToScreenTransformation){
          return number;
        } else {
          return this._assignedResolver.psedoScalar(number);
        }
      }
    }
  }
  public abstract execute(ctx: CanvasRenderingContext2D);
  protected stroke(ctx: CanvasRenderingContext2D, linewidth: number = 1, color: string = '#000', bounds?:Box2) {
    ctx.lineWidth = linewidth;
    ctx.strokeStyle = color;
    if(bounds){
      ctx.strokeRect(bounds.min.x, bounds.min.y, bounds.max.x - bounds.min.x, bounds.max.y - bounds.min.y)
    }
    else ctx.stroke();
  }
  protected fill(ctx: CanvasRenderingContext2D, color: string = '#000', bounds?: Box2) {
    ctx.fillStyle = color;
    if(bounds){
      ctx.fillRect(bounds.min.x, bounds.min.y, bounds.max.x - bounds.min.x, bounds.max.y - bounds.min.y)
    }
    else {
      ctx.fill();
    }
  }
}


