import { Injectable, Inject } from "@angular/core";
import { DrawCommand, WorldToScreenResolver } from './commands/draw-command.model';
import { ViewControlService } from "../services/view-control.service";
import { CanvasService } from '../services/canvas.service';

@Injectable()
export class DrawingCompiler{
  private readonly resolver: WorldToScreenResolver;
  public constructor(@Inject(ViewControlService) view: ViewControlService,
    @Inject(CanvasService) private canvas: CanvasService){
    this.resolver = view.factoryWorldToScreenResolver();
  }
  private drawBuffer: DrawCommand[] = [];
  /**
   * Inserts list of commands to drawing buffer
   * @param commands list of commands to add to buffer
   */
  public addCommands(commands: DrawCommand[]){
    commands.forEach(x=>x.worldToScreenResolver = this.resolver);
    this.drawBuffer.push(...commands);
  }
  /**
   * clears drawing buffer
   */
  public clearBuffer(){
    this.drawBuffer = [];
  }
  public compile(){
    this.drawBuffer.forEach(command=>{
      command.execute(this.canvas.context);
    })
  }
}
