export type DrawStyle = {
  linewidth?: number;
  color?: string;
  adjust?: number;
  fill?: boolean;
  dashed?: number[];
  borderColor?: string;
};