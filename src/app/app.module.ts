import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CanvasService } from './services/canvas.service';
import { Primitives2DFactory } from './services/primitive.factory';
import { ViewControlService } from './services/view-control.service';
import { ObjectManager } from './services/object-manager.service';
import { DrawingCompiler } from './drawing-compilation/drawing-compiler.service';
import { PluginToken } from './models/plugins/object-manager-base.model';
import { TransformPlugin } from './models/plugins/transform-plugin/transform-pluging.model';
import { ArrowService } from './services/arrows.service';
import { Gesture } from './models/controls/gesture.model';
import { EventService } from './services/event.service';
import { HotkeyService } from './services/hotkey.service';

Gesture.register('mousedrag', new Gesture('mousedrag', 'mousedown > mousemove* | mouseup'));
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    CanvasService,
    Primitives2DFactory,
    ViewControlService,
    ObjectManager,
    DrawingCompiler,
    ArrowService,
    {
      provide: PluginToken,
      useValue: [TransformPlugin]
    },
    EventService,
    HotkeyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
