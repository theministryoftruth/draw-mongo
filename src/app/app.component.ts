import { Component, ViewChild, ElementRef, Inject, ChangeDetectionStrategy } from '@angular/core';
import { Primitives2DFactory } from './services/primitive.factory';
import { Vector2 } from './models/geometry/vector2.model';
import { Box2 } from './models/geometry/box2.model';
import { ObjectManager } from './services/object-manager.service';
import { VectorPath2D } from './models/geometry/path2d.model';
import { Direction } from './utils/contants';
import { ArrowService } from './services/arrows.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

  public constructor(@Inject(Primitives2DFactory) private primitives: Primitives2DFactory,
  @Inject(ObjectManager) private manager: ObjectManager,
  @Inject(ArrowService) private arrows: ArrowService) {
    manager.canvas.canvasWillRerender.subscribe(()=>this.canvasWillUpdate())
  }
  @ViewChild('canvas') public canvasRef: ElementRef;
  public cursorStyle: string = 'default';
  public ngAfterViewInit() {
    this.manager.init(this.canvasRef.nativeElement);
    const grid = this.primitives.grid(50, "#AEBE8E"),
      rect = this.primitives.simpleRect(new Vector2(50,24), new Box2(new Vector2(50,50),new Vector2(-20, -100))),
      rect2 = this.primitives.simpleRect(new Vector2(-50,-24), new Box2(new Vector2(50,50),new Vector2(-20, -100)));
    let hotArrow;
    this.arrows.allowHotArrows().subscribe(arrow=>{
      if(hotArrow){
        this.manager.remove(hotArrow);
      }
      this.manager.add(arrow);
      hotArrow = arrow;
    })
    this.manager.add(grid);
    this.manager.canvas.trigger();

  }
  canvasWillUpdate(): any {
    this.manager.canvas.background("#EFFFCF");
    this.manager.render();
  }
}
